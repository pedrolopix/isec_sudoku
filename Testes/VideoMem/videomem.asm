; L� uma imagem do disco para a mem�ria de video
; Pedro Lopes - 21200565
; ISEC 2011
.8086
.MODEL SMALL
.STACK 2048

DADOS		SEGMENT PARA 'DATA'
;**** Inserir variaveis
string1	    db	"Ola mundo!",0
DADOS		ENDS

CODIGO		SEGMENT PARA 'CODE'
			ASSUME CS:CODIGO, DS:DADOS

ECRAN2580 PROC  
		MOV AH,0H ; Set Video Mode 25x80
		MOV AL,3H
		INT 10H 
		RET
ECRAN2580 ENDP

ESPERA_TECLA PROC
KCICLO1:
        MOV   AH,0BH            ;funcao que verifica o buffer do teclado
        INT   21H
        CMP   AL,0FFH           ;Ve se tem Tecla no Buffer
        JNE   KCICLO1        	;Enquanto n�o tem tem tecla no buffer,espera
        MOV   AH,08H            ;Funcao para ler do teclado/buffer
        INT   21H      
		RET		
ESPERA_TECLA ENDP
			
INICIO:
			MOV	AX, DADOS
			MOV DS, AX
			MOV	ES, AX	
;CODIGO
            CALL ECRAN2580
			mov ax,0B800h
			mov es,ax
		    mov di,2*(80*10+1) ; ES:DI points to row 10 / column 1 of video RAM
			mov si,offset string1 ; DS:SI points to string1
			mov ah,2Fh ; color attribute (white on green)
			cld ; clear direction flag = forwards
			
			
loop1:
			lodsb ; AL = char
			or al,al ; end of string?
			jz endloop1
			stosw ; write char + attribute to video RAM
			jmp loop1
endloop1:

FIM:
			CALL ESPERA_TECLA;
			MOV	AH,4Ch
			INT	21h

CODIGO		ENDS
END			INICIO
