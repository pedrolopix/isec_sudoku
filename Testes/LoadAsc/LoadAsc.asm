; L� UMA IMAGEM ASCCI DO DISCO
; PEDRO LOPES - 21200565
; ISEC 2011
.8086
.MODEL SMALL
.STACK 2048H

DSEG    SEGMENT PARA PUBLIC 'DATA'
        ERRO_OPEN       DB      'Erro a abrir o ficheiro','$'
        ERRO_LER_MSG    DB      'Erro a ler do ficheiro','$'
        ERRO_CLOSE      DB      'Erro a tentar fechar o ficheiro','$'
        FICH         	DB      'IMAGE.ASC',0
        HANDLEFICH      DW      0
        CAR_FICH        DB      ?
DSEG    ENDS



CSEG    SEGMENT PARA PUBLIC 'CODE'
		ASSUME  CS:CSEG, DS:DSEG

MAIN    PROC

        MOV     AX,DSEG
        MOV     DS,AX


;ABRE FICHEIRO

        MOV     AH,3DH
        MOV     AL,0
        LEA     DX,FICH
        INT     21H
        JC      ERRO_ABRIR
        MOV     HANDLEFICH,AX
        JMP     LER_CICLO

ERRO_ABRIR:
        MOV     AH,09H
        LEA     DX,ERRO_OPEN
        INT     21H
        JMP     SAI

LER_CICLO:
        MOV     AH,3FH
        MOV     BX,HANDLEFICH
        MOV     CX,1
        LEA     DX,CAR_FICH
        INT     21H
		JC		ERRO_LER
		CMP		AX,0		;EOF?
		JE		FECHA_FICHEIRO
        MOV     AH,02H
		MOV		DL,CAR_FICH
		INT		21H
		JMP		LER_CICLO

ERRO_LER:
        MOV     AH,09H
        LEA     DX,ERRO_LER_MSG
        INT     21H

FECHA_FICHEIRO:
        MOV     AH,3EH
        MOV     BX,HANDLEFICH
        INT     21H
        JNC     SAI

        MOV     AH,09H
        LEA     DX,ERRO_CLOSE
        INT     21H
SAI:
        MOV     AH,4CH
        INT     21H
MAIN    ENDP
CSEG	ENDS
END     MAIN           
