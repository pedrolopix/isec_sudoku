; L� uma imagem do disco para a mem�ria de video
; Pedro Lopes - 21200565
; ISEC 2011
.8086
.MODEL SMALL
.STACK 2048

DADOS		SEGMENT PARA 'DATA'
		ERRO_OPEN       DB  'Erro ao abrir o ficheiro','$'
        ERRO_LER_MSG    DB  'Erro ao ler do ficheiro','$'
        ERRO_CLOSE      DB  'Erro ao fechar o ficheiro','$'
		FICH	    	DB	"IMAGEM.BSV",0
        HANDLEFICH      DW  0
		HEADER			DB '????????'
		CAR_FICH        DW  0
DADOS		ENDS

CODIGO		SEGMENT PARA 'CODE'
			ASSUME CS:CODIGO, DS:DADOS
			
ECRAN2580 PROC  
		MOV AH,0H ; Set Video Mode 25x80
		MOV AL,3H
		INT 10H 
		RET
ECRAN2580 ENDP

ESPERA_TECLA PROC
KCICLO1:
        MOV   AH,0BH            ;funcao que verifica o buffer do teclado
        INT   21H
        CMP   AL,0FFH           ;Ve se tem Tecla no Buffer
        JNE   KCICLO1        	;Enquanto n�o tem tem tecla no buffer,espera
        MOV   AH,08H            ;Funcao para ler do teclado/buffer
        INT   21H      
		RET		
ESPERA_TECLA ENDP

MOSTRAFICHEIRO PROC 
        MOV     AH,3DH
        MOV     AL,0
        ;LEA     DX,FICH
        INT     21H
        JC      ERRO_ABRIR
        MOV     HANDLEFICH,AX
        MOV 	DI,0000H
        MOV     AH,3FH
        MOV     BX,HANDLEFICH
        MOV     CX,7
        LEA     DX,HEADER
        INT     21H
		JC		ERRO_LER		
		
		JMP     LER_CICLO

ERRO_ABRIR:
        MOV     AH,09H
        LEA     DX,ERRO_OPEN
        INT     21H
		RET

LER_CICLO:
        MOV     AH,3FH
        MOV     BX,HANDLEFICH
        MOV     CX,2
        LEA     DX,CAR_FICH
        INT     21H
		JC		ERRO_LER
		CMP		AX,0		;EOF?
		JE		FECHA_FICHEIRO
        
		MOV 	AX,0B800H
		MOV 	ES,AX
		MOV     BX,CAR_FICH
		MOV     AL,BL
		MOV 	ES:[DI],AL
		INC		DI
		MOV	    AL,BH
		MOV 	ES:[DI],AL
		INC		DI
	
		JMP		LER_CICLO

ERRO_LER:
        MOV     AH,09H
        LEA     DX,ERRO_LER_MSG
        INT     21H

FECHA_FICHEIRO:
        MOV     AH,3EH
        MOV     BX,HANDLEFICH
        INT     21H
		RET
		
        MOV     AH,09H
        LEA     DX,ERRO_CLOSE
        INT     21H
MOSTRAFICHEIRO ENDP			
			
INICIO:
		MOV		AX, DADOS
		MOV 	DS, AX
		MOV		ES, AX	
;CODIGO
		CALL ECRAN2580
		LEA 	DX,FICH
		CALL 	MOSTRAFICHEIRO
		CALL 	ESPERA_TECLA	

FIM:
		MOV	AH,4Ch
		INT	21h

CODIGO		ENDS
END			INICIO
