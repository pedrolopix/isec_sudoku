; TRABALHO PRATICO SODUKU
; PEDRO LOPES - 21200565
; Jorge Carvalho - 21190512
; ISEC 2011
.8086
.MODEL SMALL
.STACK 2048H

DSEG    SEGMENT PARA PUBLIC 'DATA'
        ERRO_OPEN       DB      'Erro ao abrir o ficheiro','$'
        ERRO_LER_MSG    DB      'Erro ao ler do ficheiro','$'
        ERRO_CLOSE      DB      'Erro ao fechar o ficheiro','$'
        ERRO_WRITE      DB      'Erro ao gravar o ficheiro','$'		
		ERROR_CREATE	DB		'Erro ao criar fichreiro','$'	
		ERRO_NAOIMPLE   DB      'NAO IMPLEMENTADO','$'
		ERRO_JOGO_RET	DB		'Jogo nao pode ser iniciado','$'
		JOGO_GANHOU		DB		'Ganhou o jogo parabens!$';
		JOGADA_ERRADA	DB		'Jogada nao � valida!$';
		JOGADA_LIMPA	DB		'                               $'
		NUM_FALTA_LIMPA DB		'        $'
        FICH_ENTRADA	DB      'ENTRADA.ASC',0
        FICH_MENU		DB      'MENU.ASC',0
        FICH_BACK		DB      'BACK.ASC',0
        FICH_TAB		DB      'TAB.ASC',0
		FICH_AJUDA		DB      'AJUDA.ASC',0
		FICH_JOGO		DB      'JOGO?.TXT',0 ; ? SUBSTUTUI O ? PELO NIVEL DO JOGO
		FICH_JOGO_GRAV  DB		'JOGOSAV.DAT',0; Nome do ficheiro onde o jogo � gravado 
        HANDLEFICH      DW      0 ; handle do ficheiro, usado nas procedimentos de ler ficheiro
        CAR_FICH        DB      ?; caracter lido no ficheiro
		TECLA           DB      ?; ultima tecla lida
		TAB_X_INI		DB      5; inicio do linha do tabuleiro
		TAB_Y_INI		DB      4; inicio da coluna do tabuleiro
		TAB_X_FIM		DB      22; inicio do linha do tabuleiro
		TAB_Y_FIM		DB      36; inicio da coluna do tabuleiro		
		JOGOTAB			DB      81 DUP('?') ;vector do tabuleiro do jogo 
		JOGO			DB      81 DUP('?'),'$' ;vector do jogo
		NUMJOGO			DW      0; numero do jogo
		NUMJOGOMAX		DW		1000; n�mero maximo de jogos no ficheiro
		NUM_EM_FALTA	DB		0;
		POSX			DB		5; LINHAS
		POSY			DB		4; COLUNAS coordenada Y no cursor no tabuleiro
		INDEX			DB		0; Posi�ao no vector jogo/jogotab
		COR_TAB			DB		1EH ; AZUL E AMARELO
		COR_NUMFIXOS	DB		17H ; AZUL E BRANCO
		VALIDA_NUM  	DB      '.........','$'; vector para validar jogada para debug
DSEG    ENDS

CSEG    SEGMENT PARA PUBLIC 'CODE'
		ASSUME  CS:CSEG, DS:DSEG
;********************************************************************
;      INCLUDES  
;********************************************************************	
INCLUDE UTILS.INC	
INCLUDE JOGO.INC
;INCLUDE DEBUG.INC

;********************************************************************
;      AJUDA  
;********************************************************************	
AJUDA PROC
		CALL 	LIMPA_ECRAN
		LEA     DX,FICH_AJUDA
		CALL 	MOSTRAFICHEIRO 
		CALL    ESPERA_TECLA
		RET	
AJUDA ENDP

	
;********************************************************************
;      MENU  
;********************************************************************		
MENU 	PROC
MENU_INI:
		CALL 	LIMPA_ECRAN
		LEA     DX,FICH_MENU
		CALL 	MOSTRAFICHEIRO 	
EM_MENU:		
		CALL 	LE_TECLA

		CMP 	TECLA,'0' ; NOVO JOGO debug
		JE 		MNU_NOVO_JOGO_DEBUG
		
		CMP 	TECLA,'1' ; NOVO JOGO FACIL
		JE 		MNU_NOVO_JOGO_FACIL

		CMP 	TECLA,'2' ; NOVO JOGO m�dio
		JE 		MNU_NOVO_JOGO_MEDIO

		CMP 	TECLA,'3' ; NOVO JOGO dificil
		JE 		MNU_NOVO_JOGO_DIFICIL
		
		CMP 	TECLA,'4'
		JE 		MNU_RETOMA_JOGO
		
		CMP 	TECLA,'5'
		JE		MNU_AJUDA	
		
		CMP 	TECLA,'6' ; SAIR
		JE 		FIM_MENU;	
		
		JMP 	EM_MENU		
		
MNU_NOVO_JOGO_FACIL:	
		MOV		AX,1000
		MOV     NUMJOGOMAX,AX
		LEA 	SI,FICH_JOGO
		MOV		AL,'0'
		ADD		SI,4
		MOV		[SI],AL
		CALL	NOVOJOGO
		JMP 	MENU_INI
		
MNU_NOVO_JOGO_MEDIO:	
		MOV		AX,1000
		MOV     NUMJOGOMAX,AX
		LEA 	SI,FICH_JOGO
		MOV		AL,'1'
		ADD		SI,4
		MOV		[SI],AL
		CALL	NOVOJOGO
		JMP 	MENU_INI
		
MNU_NOVO_JOGO_DIFICIL:	
		MOV		AX,1000
		MOV     NUMJOGOMAX,AX
		LEA 	SI,FICH_JOGO
		MOV		AL,'2'
		ADD		SI,4
		MOV		[SI],AL		
		CALL	NOVOJOGO
		JMP 	MENU_INI

MNU_NOVO_JOGO_DEBUG:	
		MOV		AX,1
		MOV     NUMJOGOMAX,AX
		LEA 	SI,FICH_JOGO
		MOV		AL,'D'
		ADD		SI,4
		MOV		[SI],AL		
		CALL	NOVOJOGO
		JMP 	MENU_INI	
		
MNU_RETOMA_JOGO:	
		;VERIFICA DE SE EXISTE JOGO
		MOV		AL,NUM_EM_FALTA
		CMP		AL,0
		JNE		SIGA
		PRINTXY 22,1,ERRO_JOGO_RET
		JMP 	EM_MENU	
SIGA:
		CALL	JOGA
		JMP 	MENU_INI
MNU_AJUDA:
		CALL	AJUDA
		JMP 	MENU_INI
		
FIM_MENU:		
		RET
MENU ENDP		
		
;********************************************************************
;      MAIN  
;********************************************************************
MAIN    PROC
        MOV     AX,DSEG
        MOV     DS,AX
		;INCIALIZA VARIAVEIS
		CALL    ECRAN25X80
        CALL 	LIMPA_ECRAN
		CALL    CURSOROFF
		LEA     DX,FICH_ENTRADA
		CALL 	MOSTRAFICHEIRO 
	    CALL	ESPERA_TECLA
		CALL	LE_JOGO_GRAVADO
		
		CALL	MENU

        CALL 	LIMPA_ECRAN
		CALL    CURSORON
		MOV     AH,4CH
        INT     21H
MAIN    ENDP	
	
	
CSEG	ENDS
END     MAIN           


